const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
            tamanhoLetra: 50 + "px",
        };//Fechamento return
    },//Fechamento data

    methods:{
        getNumero(numero){
            this.ajusteTamanhoDisplay();
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();    
            }
            else{
                if(this.operador == "=" || this.valorDisplay.includes("op")){
                    this.valorDisplay = "";
                    this.operador = null;
                }               
                //this.valorDisplay = this.valorDisplay + numero.toString();
                //Adição simplificada
                this.valorDisplay += numero.toString();
            }

        },//Fechamento getNumero

        limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.tamanhoLetra = 50 + "px";
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += ".";
            }//Fechamento if

        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        case "-":
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;
                        case "*":
                            this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                            break;
                        case "/":
                            if(displayAtual == 0 || this.numeroAtual == 0){
                                this.valorDisplay = "Operação impossível!"
                            }
                            else{
                                this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                            }
                            break;

                    }//Fim do switch
                    
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    if(this.operador != "="){
                        this.operador = null; 
                    }
                    //Acertar o número de casas quando o resultado for decimal.
                    if(this.valorDisplay.includes(".")){
                        const numDecimal = parseFloat (this.valorDisplay);
                        this.valorDisplay = (numDecimal.toFixed(2)).toString();
                    }
                }//Fim do if
                else{
                    this.numeroAnterior + displayAtual;
                }//Fim do else
            }//Fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != '='){
                this.valorDisplay = '0';
            }
            this.ajusteTamanhoDisplay()
        },//fim operacoes

        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 30){
                this.tamanhoLetra = 5 + "px";
            }
            if(this.valorDisplay.length >= 23){
                this.tamanhoLetra = 14 + "px";
                return;
            }
            else if(this.valorDisplay.length >= 15){
                this.tamanhoLetra = 20 + "px";
                return;
            }
            else if(this.valorDisplay.length >= 9){
                this.tamanhoLetra = 30 + "px";
                return;
            }

            else{
                this.tamanhoLetra = 50 + "px";
            }
        },

    },//Fechamento methods
}).mount("#app");//Fechamento app