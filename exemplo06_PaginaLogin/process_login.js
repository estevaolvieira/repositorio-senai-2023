const {createApp} = Vue;

createApp ({
    data(){
        return{
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        }//fechamento return
    },//fechamento data

    methods:{
        login(){
            // alert "Testandooo"

            // simulando uma requisição de login assíncrona
            setTimeout (() => {
                if((this.usuario === "Estevão" && this.senha === "12345678") ||
                (this.usuario === "oãvetsE" && this.senha ==="87654321")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";    
                    // alert("Login efetuado com sucesso!");   
                } //fim do if
                else{
                    // alert("Usuário ou Senha incorretos!")
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;

                }
            }, 1000);
        },
    },//fechamento methods
}).mount ("#app")