const {createApp} = Vue;
createApp({
    data(){
        return{
            //Variáveis que a página de cadastro precisa para funcionar
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,

            //Arrays (vetores) para armazenamento dos nomes de usuários e senhas
            usuarios: ["admin", "Estevão", "Taynah", "Taynara"],
            senhas:["1234", "1234", "1234", "1234"],
            userAdmin: false,
            mostrarEntrada: false,

            //Variáveis para tratamento das informações dos novos usuários
            newUsername: "",
            newPassword: "",
            confirmPassword: "",

            //Variáveis para a lista
            mostrarLista: false,
            
            //Variável para o login
            mostrarMenu: false,
            
            //Variável para o post instagram
            mostrarPost: false,
        };//Fechamento return
    },//fechamento data

    methods:{
        login(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;
                //Verificação de usuário e senha cadastrados ou não, nos arrays.
                const index = this.usuarios.indexOf(this.usuario);
                if (index !== -1 && this.senhas[index] === this.senha){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";
                    
                    //Registrando o novo usuário no localStorage, para lembrete de acessos
                    localStorage.setItem("usuario", this.usuario);
                    localStorage.setItem("senha", this.senha);

                    // Verificando se o usuário é admin
                    if(this.usuario === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!";
                    }
                    else{
                        this.mostrarPost = true;
                        
                    }

                }//fechamento if
                else {
                    this.sucesso = null;
                    this.erro = "Usuário e/ou senha incorretos!";
                }//fechamento else
            }, 1000);//fechamento setTimeout
            
        },//fechamento login2

        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.erro = null;
                this.sucesso = "Carregando Página de Cadastro...";
                this.mostrarEntrada = true;

                //espera estratégica antes do carregamento da página
                setTimeout(() => {}, 2000);

                setTimeout(() => {
                    window.location.href ="cadastro.html";
                }, 1000);
            }//fechamento if
            else{
                this.sucesso = null;
                setTimeout(() => {
                   this.mostrarEntrada = true;
                   this.erro = "Sem privilégios ADMIN!";
                   this.sucesso = null; 
                }, 1000);
            }//fechamento else
        },//fechamento paginaCadastro

        adicionarUsuario(){
            this.mostrarEntrada = false;
            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem("senha");
            
            setTimeout(() => {
               this.mostrarEntrada = true;
                /*verificando se o novo usuário é diferente de vazio e se ele já não está cadastrado no sistema*/
               //if (this.usuario === "admin"){                
                    if(!this.usuarios.includes (this.newUsername) && this.newUsername !== "" &&  !this.newUsername.includes(" ")){
                        //Validação da senha digitada para cadastro no array
                        if(this.newPassword !== "" && !this.newPassword.includes (" ") && this.newPassword === this.confirmPassword){
                            //Inserindo o novo usuário e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);

                            //Atualizando o usuário recém cadastrado no localStorage
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));

                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                            this.erro = null;
                            this.sucesso = "Usuário cadastrado com sucesso!";
                        }//fechamento if password

                        else{
                            this.sucesso = null;
                            this.erro = "Por favor, informe uma senha válida!";

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                        }//fechamento else
                    }//fechamento if includes
                    
                    else{
                        this.erro = "Usuário inválido! Por favor digite um usuário diferente.";
                        this.sucesso = null;

                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                    }//fechamento else
            //    }
            //    else{
            //         this.erro = "Não está logado como ADMIN!";
            //         this.sucesso = null;
            //    }               
            }, 300);
        },//fechamento adicionar usuario

        listarUsuarios(){
            //if(this.usuario == "admin"){
                if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                    this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                    this.senhas = JSON.parse(localStorage.getItem("senhas"));
                }//fechamento if
                this.mostrarLista = !this.mostrarLista;    
            // }
            // else{
            //     this.mostrarEntrada = true;
            //     this.erro = "Não está logado como ADMIN!";
            //     this.sucesso = null;
            // }
        },//fechamento listarUsuarios

        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if (usuario === "admin"){
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.erro = "O usuário admin não pode ser excluído!" 
                }, 500);
                return; //força a saída deste bloco
            }//fechamento do if adm

            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    //atualiza o array usuarios no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));

                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.erro = null;
                        this.sucesso = "Usuário excluído com sucesso!";
                    }, 500);

                }//fechamento if
            }//fechamento if user

        }//fechamento excluirUsuarios
    },//fechamento methods
}).mount("#app");//Fechamento app